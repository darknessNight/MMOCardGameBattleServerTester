package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.ConnectResponse;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.GamePhase;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.Hello;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.UserData;
import com.MMOCardGame.GameServer.Tester.ServerProvider;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCase;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.CasesCreator;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcClient;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcInitedServerTest;
import com.MMOCardGame.GameServer.Tester.TestHelpers.InjectFactories.InjectedGameSessions;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Test
public class ConnectUsersToSessionTest extends GrpcInitedServerTest {
    private int userCount;
    private boolean internalRun;

    @TestCasesList
    public static List<CaseArgs> getCases() {
        return new CasesCreator<>(CaseArgs.class)
                .addParam("userCount", Arrays.asList(2, 3, 4))
                .addParam("matchType", Arrays.asList(1, 2))
                .addParam("internalRun", Arrays.asList(true, false))
                .addFilter(caseArgs -> !(caseArgs.userCount == 3 && caseArgs.matchType == 2))
                .createCases();
    }

    @Override
    protected ServerProvider initServerProvider() {
        if (internalRun)
            return getInjectionServerProvider(new InjectedGameSessions());
        else return (getProcessServerProvider());
    }

    @Override
    public void initCase(TestCase testCase) {
        CaseArgs args = (CaseArgs) testCase;
        setMatchType(GameSessionSettings.MatchType.Single);
        setSessionId(1);
        userCount = args.userCount;
        internalRun = args.internalRun;

        List<UserData> userDataList = new ArrayList<>();
        for (int i = 0; i < userCount; i++)
            userDataList.add(getUserInfo(i));

        setUserData(userDataList);
    }

    private UserData getUserInfo(int i) {
        return UserData.newBuilder()
                .addAllCardIds(getCardIds())
                .setToken("TestToken" + i)
                .setUsername("User" + i)
                .setHeroId(1)
                .build();
    }

    public List<Integer> getCardIds() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 45; i++)
            list.add(i % 5 + 1);
        return list;
    }

    @Override
    public void runTest() {
        List<GrpcClient> grpcClients = new ArrayList<>();
        try {
            for (int i = 0; i < userCount; i++) {
                UserData userData = getUserInfo(i);
                GrpcClient grpcClient = connectToGameServer();
                ConnectResponse response = sendConnectionMessageAndGetResponse(userData, grpcClient);
                grpcClients.add(grpcClient);
                assertResponse(response);
            }
        } finally {
            grpcClients.forEach(GrpcClient::shutdown);
        }
    }

    private ConnectResponse sendConnectionMessageAndGetResponse(UserData userData, GrpcClient grpcClient) {
        return grpcClient.connect(Hello.newBuilder()
                .setUserName(userData.getUsername())
                .setUserPasswordToken(userData.getToken())
                .build());
    }

    private GrpcClient connectToGameServer() {
        GrpcClient grpcClient = new GrpcClient();
        grpcClient.start(gamePort);
        return grpcClient;
    }

    private void assertResponse(ConnectResponse response) {
        assertEquals(0, response.getResponse().getReponseCode());
        assertTrue(response.getResponse().getConnectionToken().length() >= 10);
        assertEquals(userCount, response.getGameState().getPlayersList().size());
        assertTrue(response.getGameState().getStackActionsList().isEmpty());
        assertEquals(-1, response.getGameState().getActivePlayer());
        assertEquals(GamePhase.Waiting, response.getGameState().getCurrentPhase());
        for(var player: response.getGameState().getPlayersList()){
            assertEquals(45, player.getCardsInLibraryCount());
        }
    }

    @Data
    public static class CaseArgs extends TestCase {
        public int userCount;
        private Integer matchType;
        private Boolean internalRun;
    }
}
