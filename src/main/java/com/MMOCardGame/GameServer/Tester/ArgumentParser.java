package com.MMOCardGame.GameServer.Tester;

import lombok.Getter;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

public class ArgumentParser {
    private final Logger logger = LoggerFactory.getLogger(com.MMOCardGame.GameServer.ArgumentParser.class);
    @Getter
    private List<TestCasesToRun> testsToRun = new ArrayList<>();
    @Getter
    private boolean noRun = false;
    @Getter
    private boolean runAll = true;
    @Getter
    private int parallelism = 1;
    @Getter
    private boolean fullReport = false;
    @Getter
    private boolean casesReport = false;
    @Getter
    private boolean errorReports = false;
    @Getter
    private boolean onlyInternalRun = false;

    public void parse(String[] args) {
        Options options = getOptions();
        parseArgsAndUpdateSettings(args, options);
    }

    private void parseArgsAndUpdateSettings(String[] args, Options options) {
        try {
            tryParseArgsAndUpdateSettings(args, options);
        } catch (ParseException e) {
            logger.error("Problem during argument parsing", e);
            System.err.println("Problem during argument parsing: " + e.getMessage());
            noRun = true;
        }
    }

    private void tryParseArgsAndUpdateSettings(String[] args, Options options) throws ParseException {
        CommandLine commandLine = parseArgs(args, options);
        displayHelpIfNeeded(options, commandLine);
        readTestsToRun(commandLine);
        readParallelism(commandLine);
        readReportsOptions(commandLine);
    }

    private void readReportsOptions(CommandLine commandLine) {
        if (commandLine.hasOption("f")) {
            casesReport = true;
            errorReports = true;
            fullReport = true;
        }
        if (commandLine.hasOption("e")) {
            casesReport = true;
            errorReports = true;
        }
        if (commandLine.hasOption("c"))
            casesReport = true;
        if (commandLine.hasOption("i"))
            onlyInternalRun = true;
    }

    private void readParallelism(CommandLine commandLine) {
        if (commandLine.hasOption("p")) {
            String value = commandLine.getOptionValue("p");
            try {
                tryParseParallelism(value);
            } catch (NumberFormatException e) {
                System.err.println("p argument must be > 1 integer value");
                noRun = true;
            }
        }
    }

    private void tryParseParallelism(String value) {
        parallelism = Integer.parseInt(value);
        if (parallelism < 1)
            throw new NumberFormatException();
    }

    private void readTestsToRun(CommandLine commandLine) {
        if (commandLine.hasOption("a")) {
            runAll = true;
        }
        if (commandLine.hasOption("t")) {
            try {
                testsToRun.addAll(parseTestsNames(commandLine.getOptionValues("t")));
                runAll = false;
            } catch (Throwable e) {
                System.err.println("Incorrect --tests value");
                noRun = true;
            }
        }
    }

    private Collection<? extends TestCasesToRun> parseTestsNames(String[] values) {
        List<TestCasesToRun> result = new ArrayList<>(values.length);
        for (String val : values) {
            if (!val.contains(":"))
                result.add(new TestCasesToRun(val, null));
            else {
                String[] splitString = val.split(":");
                result.add(new TestCasesToRun(splitString[0], getTestCasesFromString(splitString[1])));
            }
        }
        return result;
    }

    private List<Integer> getTestCasesFromString(String s) {
        if (!s.matches("^[0-9;\\-]+"))
            throw new RuntimeException();
        List<Integer> results = new ArrayList<>();
        String[] splitString = s.split(";");
        for (String val : splitString) {
            if (val.contains("-")) {
                getTestCasesRange(results, val);
            } else {
                results.add(Integer.parseInt(val));
            }
        }
        return results;
    }

    private void getTestCasesRange(List<Integer> results, String val) {
        String[] range = val.split("-");
        if (range.length != 2)
            throw new RuntimeException();
        int begin = Integer.parseInt(range[0]);
        int end = Integer.parseInt(range[1]);
        if (end <= begin)
            throw new RuntimeException();
        IntStream.range(begin, end + 1).forEach(results::add);
    }

    private CommandLine parseArgs(String[] args, Options options) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        return parser.parse(options, args);
    }

    private void displayHelpIfNeeded(Options options, CommandLine commandLine) {
        if (commandLine.hasOption("?")) {
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("GameServerTester", options);
            noRun = true;
        }
    }

    private Options getOptions() {
        Options options = new Options();

        options.addOption(Option.builder("?")
                .longOpt("help")
                .desc("Display this message")
                .build());

        options.addOption(Option.builder("a")
                .longOpt("runAll")
                .desc("Run all tests. Its default option")
                .build());

        options.addOption(Option.builder("t")
                .longOpt("tests")
                .valueSeparator(',')
                .hasArgs()
                .argName("TestName")
                .desc("Run selected tests. Patterns: <simply or fully qualified test name>[:list of cases] examples: Test1:1;2;3;4,Test2:1;2-5;7,Test3")
                .build());

        options.addOption(Option.builder("p")
                .longOpt("parallelism")
                .hasArg()
                .desc("Set number of thread used during executing tests")
                .build());

        options.addOption(Option.builder("f")
                .longOpt("fullReports")
                .desc("Display full test reports. Includes casesSummary and errorSummary")
                .build());

        options.addOption(Option.builder("c")
                .longOpt("casesSummary")
                .desc("Display cases summary without test output.")
                .build());

        options.addOption(Option.builder("e")
                .longOpt("errorSummary")
                .desc("Display cases summary with test output for failed and error cases. Includes casesSummary")
                .build());

        options.addOption(Option.builder("i")
                .longOpt("onlyInternalRun")
                .desc("Run only test not using new processes")
                .build());
        return options;
    }
}
