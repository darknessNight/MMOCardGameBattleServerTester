package com.MMOCardGame.GameServer.Tester;

public class OnlyInternalTestsException extends Throwable {
    public OnlyInternalTestsException(String reason) {
        super(reason);
    }
}
