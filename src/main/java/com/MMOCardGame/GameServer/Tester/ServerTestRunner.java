package com.MMOCardGame.GameServer.Tester;

public class ServerTestRunner extends TestRunner {
    public ServerTestRunner(ArgumentParser arguments) {
        super(arguments);
    }

    @Override
    protected void runTestCase(Class<? extends BaseTest> testClass, TestCase testCase, TestCaseResult testCaseResult, String caseWorkspaceDir) {
        ServerProvider serverProvider = null;
        try {
            BaseTest test = initAndGetTestCase(testClass, testCase, caseWorkspaceDir);
            serverProvider = test.getServerProvider();
            if (arguments.isOnlyInternalRun() && serverProvider.isStartingNewProcess()) {
                testCaseResult.setResult(TestResult.Result.Ignored);
                testCaseResult.setCausedBy(new OnlyInternalTestsException("Run only internal"));
                return;
            }
            serverProvider.runServer();
            try {
                super.tryRunTestCase(test, testCaseResult);
            } finally {
                serverProvider.stopServer();
            }
            test.cleanup();
        } catch (Throwable throwable) {
            testCaseResult.setResult(TestResult.Result.Error);
            testCaseResult.setCausedBy(throwable);
        } finally {
            if (serverProvider != null)
                checkServerState(testCaseResult, serverProvider);
        }
    }

    private void checkServerState(TestCaseResult testCaseResult, ServerProvider serverProvider) {
        if (serverProvider.isError()) {
            testCaseResult.setResult(TestResult.Result.Error);
            testCaseResult.setCausedBy(serverProvider.getError());
        } else if (serverProvider.exitCode() != 0) {
            testCaseResult.setResult(TestResult.Result.Error);
            testCaseResult.setCausedBy(new AssertionException("Server stopped with error code: " + serverProvider.exitCode()));
        }
        testCaseResult.setOutput(getMarkedColor() + "<Server error stream>\n" + getResetColor() + serverProvider.getErrorOutput() +
                getMarkedColor() + "\n<Server standard output>\n" + getResetColor() + serverProvider.getStandardOutput());
    }

    private String getMarkedColor() {
        return (char) 27 + "[34m";
    }

    private String getResetColor() {
        return (char) 27 + "[0m";
    }
}
