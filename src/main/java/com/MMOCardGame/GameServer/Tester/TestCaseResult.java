package com.MMOCardGame.GameServer.Tester;

import lombok.Data;

import java.util.Date;

@Data
public class TestCaseResult {
    private TestResult.Result result = TestResult.Result.Passed;
    private Throwable causedBy;
    private Date started;
    private long duration;
    private String testCase;
    private String output;
    private int caseId;
}
