package com.MMOCardGame.GameServer.Tester;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class TestCasesToRun {
    String testName;
    List<Integer> casesToRun;
}
