package com.MMOCardGame.GameServer.Tester;

import com.MMOCardGame.GameServer.common.CallbackTaskWithReturn;
import com.google.common.reflect.ClassPath;
import lombok.Setter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TestFinder {
    CallbackTaskWithReturn<Boolean, Class<?>> filter;
    @Setter
    private int parallelism;
    private ForkJoinPool forkJoinPool = null;
    private List<String> testToRun = null;

    private Predicate<TestCasesToRun> isNotEmptyTest() {
        return testCasesToRun -> testCasesToRun.getCasesToRun() == null || testCasesToRun.getCasesToRun().size() > 0;
    }

    public Set<? extends Class<? extends BaseTest>> findTests() throws IOException, ExecutionException, InterruptedException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        ClassPath classPath = ClassPath.from(classLoader);

        filter = getFilter();

        return getForkJoinPool().submit(() ->
                classPath.getTopLevelClassesRecursive("com.MMOCardGame.GameServer")
                        .parallelStream()
                        .map(ClassPath.ClassInfo::load)
                        .filter(this::classesFilter)
                        .map(this::castType)
                        .collect(Collectors.toSet())).get();
    }

    private Boolean classesFilter(Class<?> aClass) {
        return BaseTest.class.isAssignableFrom(aClass) &&
                Arrays.stream(aClass.getAnnotations())
                        .anyMatch(annotation -> annotation.annotationType().equals(Test.class))
                && filter.run(aClass);
    }

    private Class<? extends BaseTest> castType(Class<?> aClass) {
        return (Class<? extends BaseTest>) aClass;
    }

    private CallbackTaskWithReturn<Boolean, Class<?>> getFilter() {
        if (testToRun == null)
            return aClass -> true;
        else return this::matchFinderFilter;
    }

    public void setFilter(List<TestCasesToRun> testsToRun) {
        this.testToRun = testsToRun.stream().filter(isNotEmptyTest()).map(TestCasesToRun::getTestName).collect(Collectors.toList());
    }

    private boolean matchFinderFilter(Class<?> aClass) {
        return testToRun.contains(aClass.getSimpleName()) || testToRun.contains(aClass.getTypeName());
    }

    private ForkJoinPool getForkJoinPool() {
        if (forkJoinPool == null)
            forkJoinPool = new ForkJoinPool(parallelism);
        return forkJoinPool;
    }
}
