package com.MMOCardGame.GameServer.Tester.TestHelpers.InjectFactories;


import com.MMOCardGame.GameServer.GameEngine.factories.GameSessionManagerBuilder;
import com.MMOCardGame.GameServer.GameEngine.factories.GameSessionManagerFactory;
import dagger.Component;

@Component(modules = {InjectGameSessionManagerProvider.class})
public interface InjectGameSessionManagerFactory extends GameSessionManagerFactory {
    GameSessionManagerBuilder getGameSessionsManagerBuilder();
}

