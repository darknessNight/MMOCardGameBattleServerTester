package com.MMOCardGame.GameServer.Tester.TestHelpers.InjectFactories;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;
import com.MMOCardGame.GameServer.GameEngine.Sessions.UserSessionEndInfo;
import com.MMOCardGame.GameServer.GameEngine.factories.GameSessionFactory;
import com.MMOCardGame.GameServer.common.ObservableNotifier;

public class InjectionGameSessionFactory extends GameSessionFactory {
    InjectedGameSessions injectedGameSessions;

    public InjectionGameSessionFactory setInjectedGameSessions(InjectedGameSessions injectedGameSessions) {
        this.injectedGameSessions = injectedGameSessions;
        return this;
    }

    @Override
    public GameSession create(GameSessionSettings gameSessionSettings, ObservableNotifier<UserSessionEndInfo> sessionEndObservable, GameSessionManager gameSessionManager) {
        if (injectedGameSessions.contains(gameSessionSettings.getSessionId()))
            return injectedGameSessions.recreateSession(sessionEndObservable, gameSessionManager);
        return super.create(gameSessionSettings, sessionEndObservable, gameSessionManager);
    }
}
