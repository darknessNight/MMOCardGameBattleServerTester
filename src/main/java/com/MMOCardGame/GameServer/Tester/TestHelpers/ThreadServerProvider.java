package com.MMOCardGame.GameServer.Tester.TestHelpers;

import com.MMOCardGame.GameServer.GameEngine.factories.GameSessionManagerFactory;
import com.MMOCardGame.GameServer.GameServer;
import com.MMOCardGame.GameServer.Tester.ServerProvider;
import com.MMOCardGame.GameServer.factories.UserConnectionsManagerFactory;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

public class ThreadServerProvider extends ServerProvider {
    public static final String ServiceToken = "ServiceToken";
    @Setter
    private int servicePort;
    @Setter
    private int gamePort;
    @Setter
    private String workspace;
    @Setter
    private UserConnectionsManagerFactory userConnectionsManagerFactory = null;
    @Setter
    private GameSessionManagerFactory gameSessionManagerFactory = null;
    private Throwable throwable = null;
    private GameServer gameServer;
    private Thread thread;

    @Override
    public void runServer() {
        try {
            gameServer = new GameServer();
            replaceFactories();
            runGameServerThread();
            waitUntilServerInitialize();
        } catch (InterruptedException e) {
        }
    }

    private void waitUntilServerInitialize() throws InterruptedException {
        while (!gameServer.isStarted() && thread.isAlive())
            Thread.sleep(50);
    }

    private void runGameServerThread() {
        thread = new Thread(() ->
                gameServer.startBlocking((workspace + "/GameServer.jar -D userServerPort=" + gamePort +
                        " -D serviceServerPort=" + servicePort + " -D cardsPath=" + workspace + "/cards.json -D heroInfoPath=" + workspace + "/heros.json" +
                        " -D innerServiceToken=" + ServiceToken).split(" ")));
        thread.setUncaughtExceptionHandler((t, e) -> throwable = e);
        thread.start();
    }

    private void replaceFactories() {
        if (userConnectionsManagerFactory != null)
            gameServer.setUserConnectionsManagerFactory(userConnectionsManagerFactory);
        if (gameSessionManagerFactory != null)
            gameServer.setGameSessionManagerFactory(gameSessionManagerFactory);
    }

    @Override
    public void stopServer() {
        gameServer.stop();
        try {
            thread.join();
        } catch (InterruptedException e) {
            throwable = e;
        }
    }

    @Override
    public Throwable getError() {
        return throwable;
    }

    @Override
    public boolean isError() {
        return throwable != null;
    }

    @Override
    public int exitCode() {
        return 0;
    }

    @Override
    public String getErrorOutput() {
        return "";
    }

    @Override
    public String getStandardOutput() {
        return "";
    }

    @Override
    public Map<String, String> getMapOfFilesToCopy() {
        Map<String, String> stringStringMap = new HashMap<>();
        stringStringMap.put("./testData/cards.json", "cards.json");
        stringStringMap.put("./testData/heros.json", "heros.json");
        return stringStringMap;
    }

    @Override
    public boolean isStartingNewProcess() {
        return false;
    }
}
