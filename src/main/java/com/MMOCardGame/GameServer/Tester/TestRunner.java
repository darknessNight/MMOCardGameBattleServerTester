package com.MMOCardGame.GameServer.Tester;

import com.MMOCardGame.GameServer.common.CallbackTask2;
import lombok.Setter;
import org.opentest4j.AssertionFailedError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TestRunner {
    protected final ArgumentParser arguments;
    final int waitMillis = 10 * 1000 * 60;
    Logger logger = LoggerFactory.getLogger(TestRunner.class);
    @Setter
    Collection<Class<? extends BaseTest>> tests;
    @Setter
    CallbackTask2<String, Integer> progressCallback;
    @Setter
    String workspaceDir;
    AtomicInteger atomicInteger;
    Map<String, List<Integer>> casesToFilter;
    @Setter
    private int parallelism;

    public TestRunner(ArgumentParser arguments) {
        this.arguments = arguments;
    }

    public Collection<TestResult> runTests() throws ExecutionException, InterruptedException {
        ForkJoinPool forkJoinPool = new ForkJoinPool(parallelism);
        atomicInteger = new AtomicInteger();
        return forkJoinPool.submit(() -> tests
                .parallelStream()
                .map(this::runTest)
                .collect(Collectors.toList()))
                .get();
    }

    private TestResult runTest(Class<? extends BaseTest> testClass) {
        progressCallback.run(testClass.getTypeName(), atomicInteger.incrementAndGet());
        TestResult testResult = new TestResult(testClass.getTypeName(), Date.from(Instant.now()));
        long testStart = System.currentTimeMillis();
        if (isTestIgnored(testClass))
            testResult.setResult(TestResult.Result.Ignored);
        else {
            runTestCases(testClass, testResult);
        }
        testResult.setDuration(System.currentTimeMillis() - testStart);
        return testResult;
    }

    private boolean isTestIgnored(Class<? extends BaseTest> testClass) {
        return Arrays.stream(testClass.getAnnotations()).anyMatch(annotation -> annotation.annotationType().equals(Ignored.class));
    }

    private void runTestCases(Class<? extends BaseTest> testClass, TestResult testResult) {
        try {
            tryRunTestCases(testClass, testResult);
        } catch (Throwable throwable) {
            testResult.setCausedBy(throwable);
            testResult.setResult(TestResult.Result.Error);
        }
    }

    private void tryRunTestCases(Class<? extends BaseTest> testClass, TestResult testResult) {
        List<TestCaseWithId> cases = getTestCases(testClass);
        String testName = testClass.getTypeName();
        new File(workspaceDir + "/" + testName).mkdir();
        cases.forEach((testCase -> processTestCase(testClass, testResult, testCase)));
    }

    private List<TestCaseWithId> getTestCases(Class<? extends BaseTest> testClass) {
        List<TestCaseWithId> cases = new ArrayList<>();
        try {
            List<TestCaseWithId> finalCases = cases;
            List<Integer> filter = this.casesToFilter.get(testClass.getTypeName());
            if (filter == null)
                filter = this.casesToFilter.get(testClass.getSimpleName());
            List<Integer> finalFilter = filter;
            Set<Method> parentMethods = Arrays.stream(testClass.getSuperclass().getMethods())
                    .filter(method -> (method.getAnnotationsByType(TestCasesList.class).length == 1))
                    .collect(Collectors.toSet());
            Arrays.stream(testClass.getMethods())
                    .filter(method -> (method.getAnnotationsByType(TestCasesList.class).length == 1) && !parentMethods.contains(method))
                    .forEach(method -> {
                        try {
                            finalCases.addAll(getFilteredCases((Collection<? extends TestCase>) method.invoke(null), finalFilter));
                        } catch (IllegalAccessException | InvocationTargetException | ClassCastException e) {
                        }
                    });
        } finally {
            if (cases.size() == 0)
                cases = Collections.singletonList(new TestCaseWithId(new TestCase("Default"), 1));
        }
        return cases;
    }

    private Collection<? extends TestCaseWithId> getFilteredCases(Collection<? extends TestCase> invoke, List<Integer> finalFilter) {
        List<TestCaseWithId> cases = new ArrayList<>();
        int i = 1;
        for (TestCase testCase : invoke) {
            if (finalFilter == null || finalFilter.size() == 0 || finalFilter.contains(i))
                cases.add(new TestCaseWithId(testCase, i));
            i++;
        }
        return cases;
    }

    private void processTestCase(Class<? extends BaseTest> testClass, TestResult testResult, TestCaseWithId testCase) {
        TestCaseResult testCaseResult = new TestCaseResult();
        testCaseResult.setCaseId(testCase.id);
        testCaseResult.setTestCase(testCase.testCase.toString());
        if (testCase.testCase.isIgnored())
            testCaseResult.setResult(TestResult.Result.Ignored);
        else {
            String caseWorkspaceDir = findNewWorkspace(workspaceDir + "/" + testClass.getTypeName() + "/" + getSecureDirName(testCase.testCase));
            new File(caseWorkspaceDir).mkdir();
            runTimeoutTestCase(testClass, testCase.testCase, testCaseResult, caseWorkspaceDir);
        }
        testResult.addTestCaseResult(testCaseResult);
    }

    private String getSecureDirName(TestCase testCase) {
        return testCase.toString().replace(" ", "_").replaceAll("[:\\\\/<>\"|?*]", "-") + "_";
    }

    private String findNewWorkspace(String path) {
        File file = new File(path);
        if (file.exists()) {
            String newPath;
            File tmpFile;
            do {
                newPath = path + ThreadLocalRandom.current().nextLong();
                tmpFile = new File(newPath);
            } while (tmpFile.exists());
            path = newPath;
        }
        return path;
    }

    private void runTimeoutTestCase(Class<? extends BaseTest> testClass, TestCase testCase, TestCaseResult testCaseResult, String caseWorkspaceDir) {
        Thread thread = new Thread(() -> {
            long start = System.currentTimeMillis();
            runTestCase(testClass, testCase, testCaseResult, caseWorkspaceDir);
            long end = System.currentTimeMillis();
            testCaseResult.setDuration(end - start);
        });
        thread.start();
        thread.setUncaughtExceptionHandler((t, e) -> {
            if (!e.getClass().equals(AssertionException.class))
                logger.error("Error during test: " + testClass.getTypeName(), e);
        });
        try {
            thread.join(waitMillis);
        } catch (InterruptedException e) {
            testCaseResult.setResult(TestResult.Result.Error);
            testCaseResult.setCausedBy(new TimeoutException());
        }
    }

    protected void runTestCase(Class<? extends BaseTest> testClass, TestCase testCase, TestCaseResult testCaseResult, String caseWorkspaceDir) {
        BaseTest test = null;
        try {
            test = initAndGetTestCase(testClass, testCase, caseWorkspaceDir);
            tryRunTestCase(test, testCaseResult);
            test.cleanup();
        } catch (Throwable throwable) {
            testCaseResult.setCausedBy(throwable);
            testCaseResult.setResult(TestResult.Result.Error);
        } finally {
            if (test != null) {
                try {
                    test.finallyCleanup();
                } catch (Throwable throwable) {
                    testCaseResult.setCausedBy(throwable);
                    testCaseResult.setResult(TestResult.Result.Error);
                }
            }
        }
    }

    protected BaseTest initAndGetTestCase(Class<? extends BaseTest> testClass, TestCase testCase, String caseWorkspaceDir) throws Throwable {
        BaseTest test = testClass.newInstance();
        test.initCase(testCase);
        test.initWorkspace(caseWorkspaceDir);
        return test;
    }

    protected void tryRunTestCase(BaseTest test, TestCaseResult testCaseResult) throws Throwable {
        test.initServer();
        runTestCaseCode(test, testCaseResult);
    }

    protected void runTestCaseCode(BaseTest test, TestCaseResult testCaseResult) throws Throwable {
        try {
            test.runTest();
        } catch (AssertionFailedError | AssertionException throwable) {
            testCaseResult.setResult(TestResult.Result.Failed);
            testCaseResult.setCausedBy(throwable);
        } finally {
            testCaseResult.setOutput(test.getOutput());
        }
    }

    public void setCasesFilter(List<TestCasesToRun> testsToRun) {
        casesToFilter = testsToRun.stream().filter(isNotEmptyTest()).collect(Collectors.toMap(TestCasesToRun::getTestName, testCasesToRun -> {
            if (testCasesToRun.getCasesToRun() == null)
                return new ArrayList<>();
            else return testCasesToRun.getCasesToRun();
        }));
    }

    private Predicate<TestCasesToRun> isNotEmptyTest() {
        return testCasesToRun -> testCasesToRun.getCasesToRun() == null || testCasesToRun.getCasesToRun().size() > 0;
    }
}
