package com.MMOCardGame.GameServer.Tester;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

public class Tester {
    private final String defaultWorkspace = "./workspace";
    private final String shortBreakLine = "---------------";
    private final String breakLine = "--------------------------------------------------";
    private TestFinder testFinder = new TestFinder();
    private TestRunner testRunner;
    private ArgumentParser arguments = new ArgumentParser();
    private long startTime;
    private long endTime;
    private String workspace;
    private long endFindTime;
    private PrintStream standardPrintStream;
    private int testsCount;

    public void start(String[] args) throws IOException, ExecutionException, InterruptedException {
        arguments.parse(args);

        if (arguments.isNoRun())
            return;
        testRunner = new ServerTestRunner(arguments);
        standardPrintStream = System.out;
        initWorkspace();
        runTests();
    }

    private void runTests() throws IOException, ExecutionException, InterruptedException {
        startTime = System.currentTimeMillis();
        Collection<Class<? extends BaseTest>> tests = findTests(arguments);
        testsCount = tests.size();
        endFindTime = System.currentTimeMillis();
        Collection<TestResult> resultsCollections = runTests(arguments, tests);
        endTime = System.currentTimeMillis();
        printResults(resultsCollections);
    }

    private void initWorkspace() {
        workspace = defaultWorkspace;
        File file = new File(workspace);
        if (!file.exists()) {
            file.mkdir();
        } else {
            file = findNewWorkspace(file);
            Arrays.stream(file.listFiles()).forEach(subfile -> {
                try {
                    if (subfile.isDirectory())
                        FileUtils.deleteDirectory(subfile);
                    else subfile.delete();
                } catch (IOException e) {
                }
            });
        }
    }

    private File findNewWorkspace(File file) {
        if (!file.isDirectory()) {
            File tmpFile;
            do {
                workspace = defaultWorkspace + ThreadLocalRandom.current().nextLong();
                tmpFile = new File(workspace);
            } while (tmpFile.exists() && !tmpFile.isDirectory() && !tmpFile.mkdir());
            file = tmpFile;
        }
        return file;
    }

    private void printResults(Collection<TestResult> resultsCollections) {
        standardPrintStream.println(breakLine + "\nTests results\n" + breakLine);
        AtomicInteger successful = new AtomicInteger();
        AtomicInteger failed = new AtomicInteger();
        AtomicInteger error = new AtomicInteger();
        AtomicInteger ignored = new AtomicInteger();
        resultsCollections.stream().forEach(testResult -> {
            printSummary(testResult);
            switch (testResult.getResult()) {
                case Error:
                    error.incrementAndGet();
                    break;
                case Failed:
                    failed.incrementAndGet();
                    break;
                case Passed:
                    successful.incrementAndGet();
                    break;
                case Ignored:
                    ignored.incrementAndGet();
                    break;
            }
        });

        standardPrintStream.println(breakLine + "\nSummary: Ran: " + resultsCollections.size() + " Passed: " + successful + " Failed: " + failed + " Error: " + error + " Ignored: " + ignored);
        standardPrintStream.println("Overall time: " + (endTime - startTime) + "ms\nSearching time: " + (endFindTime - startTime) + "ms\nTests time: " + (endTime - endFindTime) + "ms");
        if (error.get() > 0 || failed.get() > 0)
            System.exit(1);
    }

    private void printSimpleSummary(TestResult testResult) {
        standardPrintStream.println(getResultColor(testResult.getResult()) + "[" + testResult.getResult() + "] <" + testResult.getTest() +
                ">" + getResetColor() + " Test cases count: " + testResult.getTestCaseResults().size() + ". Duration: " + testResult.getDuration() + "ms." +
                " Started: " + testResult.getStarted() + "\n\tCases passed: " + testResult.getPassedCases() + " Failed: " + testResult.getFailedCases() +
                " Error: " + testResult.getErrorCases() + " Ignored: " + testResult.getIgnoredCases());
    }

    private String getResetColor() {
        return (char) 27 + "[0m";
    }

    private void printSummary(TestResult testResult) {
        printSimpleSummary(testResult);
        if (testResult.getCausedBy() != null) {
            standardPrintStream.println("Caused by: ");
            testResult.getCausedBy().printStackTrace();
        }
        if (arguments.isCasesReport())
            testResult.getTestCaseResults().forEach(testCaseResult -> {
                standardPrintStream.println("\t" + getResultColor(testCaseResult.getResult()) + "[" + testCaseResult.getResult() + "] " + testCaseResult.getCaseId() + ". <" + testCaseResult.getTestCase() + "> "
                        + getResetColor() + "Duration: "
                        + testCaseResult.getDuration() + " ms.");
                if (testCaseResult.getCausedBy() != null && arguments.isErrorReports()) {
                    standardPrintStream.println("\tCaused by: ");
                    printCaseError(testCaseResult);
                }
                if (testCaseResult.getOutput() != null && (arguments.isFullReport() ||
                        (isTestCaseFailed(testCaseResult) && arguments.isErrorReports())))
                    standardPrintStream.println("\tTest case output:\n\t\t" + testCaseResult.getOutput().replace("\n", "\n\t\t"));
            });
    }

    private void printCaseError(TestCaseResult testCaseResult) {
        if (testCaseResult.getCausedBy().getClass().equals(OnlyInternalTestsException.class)) {
            standardPrintStream.println("\t\t\t" + getResultColor(TestResult.Result.Ignored) + testCaseResult.getCausedBy().getMessage() + getResetColor());
        } else {
            StringWriter errors = new StringWriter();
            testCaseResult.getCausedBy().printStackTrace(new PrintWriter(errors));
            standardPrintStream.println(getResultColor(TestResult.Result.Error) + errors.toString() + getResetColor());
        }
    }

    private boolean isTestCaseFailed(TestCaseResult testCaseResult) {
        return testCaseResult.getResult() == TestResult.Result.Error || testCaseResult.getResult() == TestResult.Result.Failed;
    }

    private String getResultColor(TestResult.Result result) {
        switch (result) {
            case Error:
                return (char) 27 + "[31m";
            case Failed:
                return (char) 27 + "[31m";
            case Passed:
                return (char) 27 + "[32m";
            case Ignored:
                return (char) 27 + "[33m";
        }
        return (char) 27 + "[30m";
    }

    private Collection<Class<? extends BaseTest>> findTests(ArgumentParser argumentParser) throws IOException, ExecutionException, InterruptedException {
        testFinder.setParallelism(argumentParser.getParallelism());

        if (!argumentParser.isRunAll()) {
            testFinder.setFilter(argumentParser.getTestsToRun());
        }
        return (Collection<Class<? extends BaseTest>>) testFinder.findTests();
    }

    private Collection<TestResult> runTests(ArgumentParser argumentParser, Collection<Class<? extends BaseTest>> tests) throws ExecutionException, InterruptedException {
        testRunner.setWorkspaceDir(workspace);
        testRunner.setCasesFilter(argumentParser.getTestsToRun());
        testRunner.setTests(tests);

        testRunner.setParallelism(argumentParser.getParallelism());
        testRunner.setProgressCallback(this::printProgress);

        return testRunner.runTests();
    }

    private void printProgress(String test, int testId) {
        standardPrintStream.println(shortBreakLine + "\nCurrent test: " + test + ". Overall Progress: " + testId + "/" + testsCount);
    }
}


